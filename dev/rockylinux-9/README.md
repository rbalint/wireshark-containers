# Wireshark Rocky Linux 9 Development Image

Docker base image with various compilers and dependencies pre-installed for
Wireshark builds on Rocky Linux 9.

This image is used by the “rpm-rockylinux-9” job in Wireshark's GitLab Pipelines:
https://gitlab.com/wireshark/wireshark/pipelines.

You can use it via GitLab at
https://gitlab.com/wireshark/wireshark-containers/container_registry.

# Contributing

Please submit patches to
https://gitlab.com/wireshark/wireshark-containers.
See
https://gitlab.com/wireshark/wireshark-containers#contributing
for details.
