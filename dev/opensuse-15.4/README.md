# Wireshark openSUSE 15.4 Development Image

Docker base image with various compilers and dependencies pre-installed for
Wireshark builds on openSUSE 15.4.

This image is used by the “rpm-opensuse-15.4” job in Wireshark's GitLab Pipelines:
https://gitlab.com/wireshark/wireshark/pipelines.

You can use it via GitLab at
https://gitlab.com/wireshark/wireshark-containers/container_registry.

# Contributing

Please submit patches to
https://gitlab.com/wireshark/wireshark-containers.
See
https://gitlab.com/wireshark/wireshark-containers#contributing
for details.
